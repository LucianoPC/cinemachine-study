﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Animator animator;
    public AxisMovement axisMovement;

    private bool isWaveArmEnabled = false;


    public void ChangeAnimationWaveArm () {
        SetAnimationWaveArm (!this.isWaveArmEnabled);
    }

    public void SetAnimationWaveArm (bool enabled) {
        this.isWaveArmEnabled = enabled;
        this.animator.SetBool ("waveArm", enabled);

        this.axisMovement.enabled = !this.isWaveArmEnabled;
    }
}
