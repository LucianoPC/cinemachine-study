﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class AxisMovement : MonoBehaviour {
    
    public float speed = 1;
    public string animatorVelocityName = "";
    public Animator animator;
    public Transform origin;

    public Vector3 Axis { get { return GetAxis (); } set { SetAxis (value); } }

    private float animationVelocity;
    private new Rigidbody rigidbody;
    [SerializeField] private Vector3 axis;


    void Awake () {
        this.rigidbody = GetComponent<Rigidbody> ();
    }

    void FixedUpdate () {
        Move ();
    }

    void OnValidate () {
        SetAxis (this.axis);
    }

    private void Move () {
        this.rigidbody.velocity = this.axis * this.speed;

        if(this.axis.x != 0f || this.axis.z != 0f)
            this.transform.forward = this.axis;

        if (this.animator != null)
            this.animator.SetFloat (this.animatorVelocityName, this.animationVelocity);
    }

    public void SetAxis (Vector3 axis) {
        SetAxis (axis.x, axis.y);
    }

    public void SetAxis (float x, float z) {
        this.axis.x = x;
        this.axis.z = z;
        this.axis.y = 0f;

        if (this.origin != null) {
            this.axis = this.origin.transform.TransformDirection (this.axis);
            this.axis.y = 0f;
        }

        this.axis.Normalize ();

        this.animationVelocity = Mathf.Lerp (0f, this.speed, this.axis.magnitude);
    }

    public Vector3 GetAxis () {
        return new Vector3 (this.axis.x, this.axis.y, this.axis.z);
    }
}
