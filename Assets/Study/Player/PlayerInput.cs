﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Player))]
public class PlayerInput : MonoBehaviour {

    public string axisVertical = "";
    public string axisHorizontal = "";
    public string buttonWaveArm = "";

    private Player player;


    void Awake () {
        this.player = GetComponent<Player> ();
    }

    void LateUpdate () {
        Movement ();
        WaveArm ();
    }

    private void Movement () {
        float axisX = string.IsNullOrEmpty (this.axisHorizontal) ? 0f : Input.GetAxis (this.axisHorizontal);
        float axisZ = string.IsNullOrEmpty (this.axisVertical) ? 0f : Input.GetAxis (this.axisVertical);

        this.player.axisMovement.SetAxis (axisX, axisZ);
    }

    private void WaveArm () {
        if (!string.IsNullOrEmpty (this.buttonWaveArm) && Input.GetButtonDown (this.buttonWaveArm))
            this.player.ChangeAnimationWaveArm ();
    }
}
